from flask import Flask
from flask import request


# creates a Flask application, named app
app = Flask(__name__)


import docsim

@app.route("/", methods=['GET'])
def display_bot_message():
    user_input1 = request.args.get('msg1')
    user_input2 = request.args.get('msg2')

    if user_input1 is not None and user_input2 is not None:
        return docsim.response(user_input1, user_input2)
    return docsim.welcome_msg()


# run the application
if __name__ == "__main__":
    app.run(debug=True)