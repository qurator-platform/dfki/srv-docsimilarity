import spacy


def response(user_input1, user_input2):
    nlp = spacy.load('en_core_web_lg')
    doc1 = nlp(user_input1)
    doc2 = nlp(user_input2)

    return 'The document similarity between sentence 1: \n' + user_input1 + '\n' +'and sentence 2: \n' + user_input2 + '\nis: {}'.format(doc1.similarity(doc2))


def welcome_msg():
    '''
    Returns a welcome/info text about the service.
    '''
    msg = 'This is a simple document similarity recognizer based on spacy-library.\n ' \
          'The idea is that the documents are respresented as vectors of features, and the documents are compared by measuring the distance between these features. \n' \
          'In particular, it computes the cosine similarity with vectors averaged over the document.\n For testing, add to URL: "/?msg1=INPUT_TEXT1&msg2=INPUT_TEXT2"'
    return msg